{-# LANGUAGE PackageImports #-}

module Main where

import Protolude
import qualified BandwidthMonitor as Monitor

main :: IO ()
main = Monitor.main
