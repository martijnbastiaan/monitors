{-# LANGUAGE PackageImports #-}

module Main where

import Protolude
import qualified IpMonitor as Monitor

main :: IO ()
main = Monitor.main
