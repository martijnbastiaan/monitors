#!/bin/bash
set -euf -o pipefail

cd "$(dirname $0)/.."

ETH=eth3
SLEEP_FOR=30
TELEGRAM_BOT_TOKEN="${TELEGRAM_BOT_TOKEN}"

REMOTE="ubnt@192.168.1.1"
REMOTE_SCRIPT_DST="/tmp/${ETH}_monitor.sh"
REMOTE_SCRIPT="#!/bin/bash
while true; do
  ip -s link show ${ETH} \
    | tail -n 4 \
    | awk '{\$1=\$1};1' \
    | grep -o -E '^[0-9]+' \
    | tr '\n' ' '
  date -Iseconds | sed -E 's/\+([0-9]{2})([0-9]{2})/+\1:\2/'
  sleep ${SLEEP_FOR}
done"

# Build project
stack build

# Copy script to remote
echo "${REMOTE_SCRIPT}" | ssh "${REMOTE}" "cat - > ${REMOTE_SCRIPT_DST}"

# Run monitor on remote. Concerning the pipes: makes sure everything terminates
# correctly.
PIPE_DST="/tmp/${ETH}_monitor"
rm -f "${PIPE_DST}"
mkfifo "${PIPE_DST}"
ssh "${REMOTE}" "bash ${REMOTE_SCRIPT_DST}" > "${PIPE_DST}" &

# Kill all background processes when exiting
trap "kill -9 $(jobs -p)" EXIT

# Run monitor
stack run monitor-eth3 < "${PIPE_DST}"
rm -f "${PIPE_DST}"
exit 1
