{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE ImplicitParams #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE NumericUnderscores #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables#-}
{-# LANGUAGE TypeFamilies #-}

{-# OPTIONS_GHC -fno-warn-orphans #-}

module Telegram where

import Protolude

-- base
import System.Environment (lookupEnv)

-- telegram-bot-simple
import qualified Telegram.Bot.API as Telegram
import Telegram.Bot.API
  (GetUpdatesRequest(..), SendMessageRequest(..), Response(..))

-- text
import qualified Data.Text as Text

-- co-log
import Colog (logDebug, logInfo)

-- me
import Util (ppShowT)
import Logging (LogEnv)

-- servant-client
import Servant.Client (ClientM, ClientError)

data TelegramContext = TelegramContext
  { tcToken :: Telegram.Token
  , tcChatId :: Telegram.SomeChatId }
  deriving (Show)

type HiddenTelegramContext = (?tgContext :: TelegramContext)

deriving instance Show Telegram.SomeChatId

telegramTokenVar :: IsString a => a
telegramTokenVar = "TELEGRAM_BOT_TOKEN"

telegramChatIdVar :: IsString a => a
telegramChatIdVar = "TELEGRAM_CHAT_ID"

getTelegramContext :: forall m env. LogEnv env m => ExceptT Text m TelegramContext
getTelegramContext = do
  lift (logDebug ("Looking up " <> telegramTokenVar <> ".."))

  telegramToken <-
    liftIO (lookupEnv telegramTokenVar) >>= \case
      Just token -> pure (Telegram.Token (Text.pack token))
      Nothing -> throwE (telegramTokenVar <> " not set")

  let
    runBot :: ClientM a -> ExceptT Text m (Either ClientError a)
    runBot = liftIO . Telegram.defaultRunBot telegramToken

  lift (logDebug "Checking token..")
  meE <- runBot Telegram.getMe
  either (throwE . ("Token check failed: " <>) . ppShowT) (const (pure ())) meE

  lift (logDebug "Getting chat id..")
  let updateReq = Telegram.GetUpdatesRequest {
    getUpdatesOffset = Nothing
  , getUpdatesLimit = Just 1
  , getUpdatesTimeout = Nothing
  , getUpdatesAllowedUpdates = Just [Telegram.UpdateMessage] }

  updatesE <- runBot (Telegram.getUpdates updateReq)
  updates <- either (throwE . ("Updates failed: " <>) . ppShowT) pure updatesE
  let chatIds = mapMaybe Telegram.updateChatId (Telegram.responseResult updates)
  chatIdEnv <- liftIO (join . fmap readMaybe <$> lookupEnv telegramChatIdVar)
  case (chatIds, chatIdEnv) of
    (_, Just chatId) -> do
      -- Got a chat id through environment file
      lift (logDebug ("Got " <> ppShowT chatId))
      let someChatId = Telegram.SomeChatId (Telegram.ChatId chatId)
      pure (TelegramContext telegramToken someChatId)
    (chatId:_, _) -> do
      -- Got a chat id through getUpdates API
      lift (logDebug ("Got " <> ppShowT chatId))
      pure (TelegramContext telegramToken (Telegram.SomeChatId chatId))
    _ ->
      throwE "Failed to retrieve chat id. Did you talk the bot in the last 24 hours?"

unsafeGetTelegramContext :: LogEnv env m => m TelegramContext
unsafeGetTelegramContext = runExceptT getTelegramContext >>= \case
  Left err -> panic err
  Right ctx -> pure ctx

makeMsg :: HiddenTelegramContext => Text -> Telegram.SendMessageRequest
makeMsg msg = Telegram.SendMessageRequest
  { sendMessageChatId = tcChatId ?tgContext
  , sendMessageText = msg
  , sendMessageParseMode = Nothing
  , sendMessageDisableWebPagePreview = Just True
  , sendMessageDisableNotification = Just False
  , sendMessageReplyToMessageId = Nothing
  , sendMessageReplyMarkup = Nothing }

unsafeSendMsg ::
  (LogEnv env m, HiddenTelegramContext) =>
  -- | Number of retries
  Int ->
  -- | Message to send
  Text ->
  m ()
unsafeSendMsg retries msg = sendMsg retries msg >>= \case
  Left err -> panic (ppShowT err)
  Right _ -> pure ()

sendMsg ::
  (LogEnv env m, HiddenTelegramContext) =>
  -- | Number of retries
  Int ->
  -- | Message to send
  Text ->
  m (Either ClientError (Response Telegram.Message))
sendMsg retries msg = go retries
 where
  go n = do
    logInfo $ "Sending Telegram message: " <> msg

    let tMsg = Telegram.sendMessage (makeMsg msg)
    reply <- liftIO (Telegram.defaultRunBot (tcToken ?tgContext) tMsg)
    case (n, reply) of
      (0, _) -> pure reply
      (_, Right _) -> pure reply
      (_, _) -> do
        liftIO (threadDelay 5_000_000) -- 5 seconds
        logInfo $ "Failed to send message! Retrying.. (" <> show n <> " tries left)"
        go (n - 1)
