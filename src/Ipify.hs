{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeApplications #-}

module Ipify where

-- protolude
import Protolude

-- me
import Logging (LogEnv)
import Util (ppShowT)

-- http-conduit
import qualified Network.HTTP.Simple as Http

-- ip
import Net.IPv4 (IPv4)
import qualified Net.IPv4 as Ipv4

-- co-log
import Colog (logInfo, logWarning, logDebug)

-- text
import qualified Data.Text as Text

-- network
import qualified Network.Socket as Network

ipifyReq :: Http.Request
ipifyReq = Http.parseRequest_ (Text.unpack ipifyUrl)

ipifyUrl :: Text
ipifyUrl = "https://api.ipify.org"

-- | Reverse DNS lookup
getHostname :: IPv4 -> IO (Maybe Text)
getHostname =
    fmap (fmap Text.pack . fst)
  . Network.getNameInfo [] True False
  . Network.SockAddrInet 80
  . Network.tupleToHostAddress
  . Ipv4.toOctets

-- Get current IP using ipify.org
getIp :: (LogEnv env m, MonadIO m) => m (Maybe IPv4)
getIp = do
  logDebug $ "Querying " <> ipifyUrl <> " for public ip.."
  respE <- liftIO (try @Http.HttpException (Http.httpBS ipifyReq))

  case respE of
    Left err -> do
      logWarning $ "HttpException:\n" <> show err
      pure Nothing
    Right resp -> do
      let
        status = Http.getResponseStatusCode resp
        body = decodeUtf8' (Http.getResponseBody resp)
      case (status, body) of
        (200, Right ip) -> do
          logInfo $ ip
          pure (Ipv4.decode ip)
        (200, Left err) -> do
          logWarning $ "Got non-utf8 response:\n" <> ppShowT err
          logWarning $ "Response was:\n" <> ppShowT resp
          pure Nothing
        _ -> do
          logWarning $ "api.ipify.org down? Got non-200 response: " <> ppShowT resp
          pure Nothing
