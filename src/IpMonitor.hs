{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE NumericUnderscores #-}
{-# LANGUAGE ImplicitParams #-}
{-# LANGUAGE QuasiQuotes #-}

module IpMonitor where

-- protolude
import Protolude

-- me!
import Logging
import qualified Telegram
import Telegram (HiddenTelegramContext)
import Ipify (getHostname, getIp)

-- ip
import Net.IPv4 (IPv4)
import qualified Net.IPv4 as Ipv4

-- interpolate
import Data.String.Interpolate (i)

previousIpFile :: FilePath
previousIpFile = "/dev/shm/previous_ip"

getPreviousIp :: IO (Maybe IPv4)
getPreviousIp = do
  prevIpE <- try @SomeException (readFile previousIpFile)
  case prevIpE of
    Left _ -> pure Nothing
    Right prevIp -> pure (Ipv4.decode prevIp)

setPreviousIp :: IPv4 -> IO ()
setPreviousIp = writeFile previousIpFile . Ipv4.encode

checkIp :: (HiddenTelegramContext, LogEnv env m) => Maybe IPv4 -> m ()
checkIp Nothing =
  pure ()
checkIp (Just ip) = do
  prevIpM <- liftIO getPreviousIp
  case prevIpM of
    Just prevIp -> do
      let
        ipT = Ipv4.encode ip
        prevIpT = Ipv4.encode prevIp

      ipRev <- liftIO (fromMaybe "?" <$> getHostname ip)
      prevIpRev <- liftIO (fromMaybe "?" <$> getHostname prevIp)

      when
        (ip /= prevIp)
        (Telegram.unsafeSendMsg 5 [i|
          Changed IP from: #{prevIpT} [#{prevIpRev}] to #{ipT} [#{ipRev}]
        |])
    Nothing ->
      pure ()

  liftIO (setPreviousIp ip)


main :: IO ()
main = do
  tgContext <- withLogEnv Telegram.unsafeGetTelegramContext
  let ?tgContext = tgContext

  forever $ do
      () <- withLogEnv (getIp >>= checkIp)
      threadDelay 5_000_000 -- 5 seconds
