{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE ImplicitParams #-}
{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE ViewPatterns #-}

module BandwidthMonitor where

-- protolude
import Protolude

-- containers
import qualified Data.Sequence as Seq
import Data.Sequence (Seq((:|>), (:<|)))

-- text
import qualified Data.Text.Lazy.IO as LText
import qualified Data.Text.Lazy as LText
import qualified Data.Text as Text

-- time
import Data.Time (UTCTime, NominalDiffTime)
import qualified Data.Time as Time

-- me!
import Util (parseZonedTime, readMaybeT, ppShowBytes)
import Logging
import qualified Telegram
import Telegram (HiddenTelegramContext)

-- co-log
import Colog (logDebug, logInfo)

type Bytes = Word
type Offset = Word
type WarningThreshold = Word

type BandwidthLog = (Seq (UTCTime, Bytes), Offset, WarningThreshold)

s128MiB :: Word
s128MiB = 128 * 1024 * 1024

minThreshold :: Word
minThreshold = s128MiB

logSize :: NominalDiffTime
logSize = Time.nominalDay

logSizeHumanReadable :: Text
logSizeHumanReadable = show (approxHours :: Word) <> " hours"
 where
  approxHours = floor (Time.nominalDiffTimeToSeconds logSize) `quot` 3600

parseLine :: Text -> Maybe (UTCTime, Bytes)
parseLine t =
  case Text.splitOn " " (Text.strip t) of
    [rxBytesT, txBytesT, timestampT] -> do
      timestamp <- parseZonedTime timestampT
      txBytes <- readMaybeT txBytesT
      rxBytes <- readMaybeT rxBytesT
      pure (Time.zonedTimeToUTC timestamp, rxBytes + txBytes)
    _ ->
      Nothing

unsafeParseLine :: HasCallStack => Text -> (UTCTime, Bytes)
unsafeParseLine t =
  case parseLine t of
    Nothing -> panic ("Failed to parse: " <> show t)
    Just tup -> tup

removeOldEntries :: BandwidthLog -> IO BandwidthLog
removeOldEntries (bLog0, offset, threshold) = do
  now <- Time.getCurrentTime
  let bLog1 = Seq.dropWhileL ((>= logSize) . Time.diffUTCTime now . fst) bLog0
  pure (bLog1, offset, threshold)

-- | Only sends warning if timestamp is less than 60 seconds ago
sendWarning :: (LogEnv env m, HiddenTelegramContext) => UTCTime -> Bytes -> m ()
sendWarning timestamp used = do
  now <- liftIO Time.getCurrentTime
  if (Time.diffUTCTime now timestamp <= Time.secondsToNominalDiffTime 60) then do
    Telegram.unsafeSendMsg 5 $
      "Warning: used " <> ppShowBytes used <> " in the last " <> logSizeHumanReadable
  else
    logDebug "Ignored warning: threshold passed more than 60 seconds ago"

processNew ::
  (HiddenTelegramContext, LogEnv env m) =>
  BandwidthLog ->
  (UTCTime, Bytes) ->
  m BandwidthLog
processNew bLog0 (newTimestamp, newBytes0) = do
  (seqLog0, offset, threshold) <- liftIO (removeOldEntries bLog0)
  let
    newBytes1 = newBytes0 + offset
    seqLog1 = seqLog0 :|> (newTimestamp, newBytes1)
    bLog1 = (seqLog1, offset, threshold)

  case seqLog0 of
    ((_, headBytes) :<| _) :|> (_, lastBytes) -> do
      let usedBytes = newBytes1 - headBytes
      logInfo ("Used: " <> ppShowBytes usedBytes)
      if | newBytes1 < lastBytes -> do
            -- Newest bytecount is _lower_ than last recorded one, meaning the
            -- router has been restarted - resetting the counters. Even though
            -- bytes recorded between the last recording and the reset, we do a best
            -- effort by making the last bytes the new offset.
            processNew (seqLog0, lastBytes, threshold) (newTimestamp, newBytes0)
         | usedBytes > 4 * threshold -> do
            -- No use in spamming
            processNew
              (seqLog0, lastBytes, threshold * 2)
              (newTimestamp, newBytes0)
         | usedBytes > 2 * threshold -> do
            -- Warn!
            sendWarning newTimestamp usedBytes
            pure (seqLog1, offset, 2 * threshold)
         | usedBytes < threshold && threshold > minThreshold -> do
             -- Adjust threshold, rerun 'processNew' with updated one
            processNew
              (seqLog0, lastBytes, threshold `quot` 2)
              (newTimestamp, newBytes0)
         | otherwise -> do
             -- No threshold crosses
            pure bLog1
    _ ->
      pure bLog1

processLine ::
  (HiddenTelegramContext, LogEnv env m) =>
  BandwidthLog ->
  Text ->
  m BandwidthLog
processLine bLog = processNew bLog . unsafeParseLine

main :: IO ()
main = do
  tgContext <- withLogEnv Telegram.unsafeGetTelegramContext
  let ?tgContext = tgContext

  lazyLines <- LText.lines <$> LText.getContents
  _bLog <-
    withLogEnv $
      foldM
        processLine
        (mempty, 0, minThreshold)
        (map LText.toStrict lazyLines)

  pure ()
