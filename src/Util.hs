module Util where

-- protolude
import Protolude

-- base
import Text.ParserCombinators.ReadP (ReadP, readP_to_S)

-- text
import qualified Data.Text as Text

-- time
import Data.Time (ZonedTime)
import qualified Data.Time.Format.ISO8601 as Time

-- hrfsize
import System.HrfSize (FileSize(..), convertSize)

-- pretty-show
import Text.Show.Pretty (ppShow)

parseReader :: ReadP t -> Text -> Maybe t
parseReader readp s = listToMaybe  [t | (t, "") <- readP_to_S readp (toS s)]

parseZonedTime :: Text -> Maybe ZonedTime
parseZonedTime = parseReader (Time.formatReadP Time.iso8601Format)

readMaybeT :: Read a => Text -> Maybe a
readMaybeT = readMaybe . toS

ppShowT :: Show a => a -> Text
ppShowT = Text.pack . ppShow

pprint :: Show a => a -> IO ()
pprint = putStrLn . ppShow

ppShowBytes :: Word -> Text
ppShowBytes bytes =
  case convertSize (fromIntegral bytes) of
    Bytes _ -> show bytes <> " Bytes"
    KiB kibs -> show kibs <> " KiB"
    MiB mibs -> show mibs <> " MiB"
    GiB gibs -> show gibs <> " GiB"
    TiB tibs -> show tibs <> " TiB"
