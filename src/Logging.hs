{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE FlexibleContexts #-}

module Logging where

import Protolude

import Colog

type LogEnv env m =
  ( WithLog env Message m
  , MonadIO m
  , HasCallStack )

withLogEnv :: LoggerT Message IO a -> IO a
withLogEnv action = do
  let logAction = cmap fmtMessage logTextStdout
  usingLoggerT logAction action
